const Boom = require('boom')
const ext = require('commander')
const jsonwebtoken = require('jsonwebtoken')
const http = require('http')
const axios = require ('axios')
const express = require('express')

// WEBSOCKET

const WebSocketServer = require('websocket').server

ext.
  version(require('../package.json').version).
  option('-s, --secret <secret>', 'Extension secret').
  option('-c, --client-id <client_id>', 'Extension client ID').
  parse(process.argv)

const secret = process.env.ENV_SECRET

const expressApp = express()
expressApp.use(express.json())
expressApp.use(express.urlencoded({ extended: true }))

expressApp.post('/question', questionController)

const server = http.createServer(expressApp)

server.listen(8081, function() {
  console.log((new Date()) + ' Server is listening on port 8081')
});

wsServer = new WebSocketServer({
  httpServer: server,
  // You should not use autoAcceptConnections for production
  // applications, as it defeats all standard cross-origin protection
  // facilities built into the protocol and the browser.  You should
  // *always* verify the connection's origin and decide whether or not
  // to accept it.
  autoAcceptConnections: false
});

function originIsAllowed(origin) {
  const acceptedOrigins = [
    'http://localhost:8081'
  ]
  return acceptedOrigins.indexOf(origin) !== -1
}

const connexions = []

function broadcast (message) {
  console.log('📡 Broadcasting to '+ connexions.length + ' active connections...')
  if (connexions.length > 0) {
    connexions.forEach(c => {
      c.send(message)
    })
  }
}

wsServer.on('request', function(request) {
  if (!originIsAllowed(request.origin)) {
    // Make sure we only accept requests from an allowed origin
    request.reject()
    console.log('🚨 Connection from origin ' + request.origin + ' rejected.')
    return
  }
  var connection = request.accept('echo-protocol', request.origin)
  console.log('📡 Connection accepted from ', request.origin)
  if (connexions.indexOf(connection) === -1) {
    connexions.push(connection)
  }
  connection.on('message', function(message) {
      if (message.type === 'utf8') {
          const data = JSON.parse(message.utf8Data)
          switch(data.type) {
            case 'vote':
              handleUserVote(connection, data)
              break
          }
      }
  })
  connection.on('close', function(reasonCode, description) {
      console.log('📡 Peer ' + connection.remoteAddress + ' disconnected.')
  })
})

function verifyAndDecodeToken (token) {
  if (token) {
    try {
      return jsonwebtoken.verify(token, secret, { algorithms: ['HS256'] })
    }
    catch (ex) {
      throw Boom.unauthorized('Invalid token')
    }
  }
  throw Boom.unauthorized('Invalid auth header')
}

///////////////////////////////////////////////

let votes = null

function handleUserVote (connection, vote) {
  if (vote.token) {
    const allowed = verifyAndDecodeToken(vote.token)
    if (allowed) { // token vérifié
      // @todo: vérifier que l'user ne vote pas 2 fois
      console.log('🗳️ ==> VOTE : ',allowed.opaque_user_id, 'a voté ', vote.vote)
      if (votes[vote.vote] && votes[vote.vote].votes.indexOf(allowed.opaque_user_id) === -1) {
        votes[vote.vote].votes.push(allowed.opaque_user_id)
      }
      const response = {
        type: 'voteConfirmation',
        vote: vote
      }
      connection.send(JSON.stringify(response))
    } else { // le token n'a pas pu être vérifié
      const response = {
        type: 'voteRejected',
        message: 'Token verification failed...'
      }
      connection.send(JSON.stringify(response))
      return
    }
  } else { // la requête ne contient pas de token
    const response = {
      type: 'voteRejected',
      message: 'Missing token...'
    }
    connection.send(JSON.stringify(response))
      return
  }
}

let currentQuestion = null
function setQuestion (question) {
  if (!!question) {
    currentQuestion = question
    setupVotes(question.responses)
    startTimer()
    const data = {
      type: 'newQuestion',
      question: question
    }
    broadcast(JSON.stringify(data))
  }
}

function setupVotes (responses) {
  votes = {}
  newVote = {}
  responses.forEach(response => {
    newVote[response.num_response] = {
      scene: response.scene,
      votes: []
    }
  })
  votes = newVote
}

let timer = null

function startTimer () {
  timer = setTimeout(endVote, 30000)
}

function endVote () {
  console.log('🗳️ ==> STOP THE COUNT !')
  timer = null
  countVotes()
}

function countVotes () {
  console.log('🗳️ ==> Counting votes...')
  const body = []
  for (const r in votes) {
    const e = {
      scene: votes[r].scene,
      votes: votes[r].votes.length
    }
    body.push(e)
  }
  console.log('🗳️ ==> Vote results.')
  console.log(body)
}

function questionController (req, res) {
  console.log('✅ Received new Question from API !')
  res.status(201).send('Question successfully received')
  if (req.body && req.body.question) {
    const question = JSON.parse(req.body.question)
    setQuestion(question)
  } else {
    res.status(400).send('Wrong parameters')
  }
}
